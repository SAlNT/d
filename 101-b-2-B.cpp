#include <bits/stdc++.h>
#define ll long long
#define ld long double
using namespace std;
const ld pi = 3.141592653589793;
int main(){
    ios::sync_with_stdio(0);
    int need;
    cin >> need;

    int n = 50000;
    int m = (n+1)/2;
    int k = 1;
    bool DEBUG = false;
    vector <int> a;
    int z;
    while (true){
        ld e = 2.0*pi*k - (int)(2.0*pi*k);
        if (e < pi/(2*m))
            break;
        z = (int)(2.0*pi*k);
        if (e > 1.0 - pi/(2*m)){
            int x[m+1];
            for (int i = 1; i <= m; i++){
                x[i] = (z+1)*i;
                if (i == 1 && n%2 == 0){
                    //cout << x[i] << "\n";
                    a.push_back(x[i]);
                    continue;
                }
                //cout << x[i] << "\n" << -x[i] << "\n";
                a.push_back(x[i]);
                a.push_back( -x[i]);
            }
            //cout << 0;
            a.push_back(0);
            sort(a.begin(), a.end());
            for (int i = 0; i < need; i++){
                cout << a[i] << "\n";
            }
            if (DEBUG){

                for (int i = 0 ;i < need; i++){
                    cout << sin(a[i]) << " ";
                }
                cout << a[a.size()-1];
            }
            return 0;
        }
        k++;
    }
    int x[m+1];
    for (int i = 1; i <= m; i++){
        x[i] = z*i;
        if (i == 1 && n%2 == 0){
            //cout << x[i] << "\n";
            a.push_back(x[i]);
            continue;
        }
        //cout << x[i] << "\n" << -x[i] << "\n";
        a.push_back(x[i]);
        a.push_back(-x[i]);
    }
    //cout << 0;
    a.push_back(0);
    sort(a.begin(), a.end());
    for (int i = 0; i < need; i++){
        cout << a[i] << "\n";
    }
    if (DEBUG){

        for (int i = 0 ;i < need; i++){
            cout << sin(a[i]) << " ";
        }
        cout << a[a.size()-1];
    }
    return 0;
}
